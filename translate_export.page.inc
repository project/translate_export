<?php

/**
 * @file
 * Pages callback for translate_export.module.
 */

function translate_export_locale_translate_seek_screen(){
	module_load_include('inc', 'locale', 'locale.admin');
  // Add CSS.
  drupal_add_css(drupal_get_path('module', 'locale') . '/locale.css');
  drupal_add_css(drupal_get_path('module', 'translate_export') . '/translate_export.css');
  $elements = drupal_get_form('locale_translation_filter_form');
  $output = drupal_render($elements);
  $output .= _translate_export_locale_translate_seek();
  return $output;
}

/**
 * User interface for the translation export screen.
 */
function translate_export_locale_translate_export_string_screen($lid) {
  // Get all languages, except English
  drupal_static_reset('language_list');
  $names = locale_language_list('name');
  unset($names['en']);
  $output = '';
  // Offer translation export if any language is set up.
  if (count($names)) {
    $elements = drupal_get_form('translate_export_locale_translate_export_string_po_form', $names , $lid);
    $output .= drupal_render($elements);
  }

  return $output;
}

/**
 * Form to export PO files for the languages provided.
 *
 * @param $names
 *   An associate array with localized language names
 */
function translate_export_locale_translate_export_string_po_form($form, &$form_state, $names, $lid) {
  $source = db_query('SELECT source, context, textgroup, location FROM {locales_source} WHERE lid = :lid', array(':lid' => $lid))->fetchObject();
  if (!$source) {
    drupal_set_message(t('String not found.'), 'error');

    return '';
  }

  $form['export_title'] = array('#type' => 'item',
    '#title' => t('Export translation'),
  );
  $form['langcode'] = array('#type' => 'select',
    '#title' => t('Language name'),
    '#options' => $names,
    '#description' => t('Select the language to export in Gettext Portable Object (<em>.po</em>) format.'),
  );
  $form['group'] = array(
    '#type'  => 'value',
    '#value' => $source->textgroup,
  );
  $form['lid'] = array(
    '#type' => 'value',
    '#value' => $lid,
  );
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Export'));
  return $form;
}

/**
 * Process a translation (or template) export form submission.
 */
function translate_export_locale_translate_export_string_po_form_submit($form, &$form_state) {
  // If template is required, language code is not given.
  $language = NULL;
  if (isset($form_state['values']['langcode'])) {
    $languages = language_list();
    $language = $languages[$form_state['values']['langcode']];
  }
  _locale_export_po($language, _locale_export_po_generate($language, _translate_export_locale_export_string_get_strings($language, $form_state['values']['group'],$form_state['values']['lid'])));
}


/**
 * User interface for the translation export screen.
 */
function translate_export_locale_translate_export_selected_screen() {
  // Get all languages, except English
  drupal_static_reset('language_list');
  $names = locale_language_list('name');
  unset($names['en']);
  $output = '';
  // Offer translation export if any language is set up.
  if (count($names)) {
    $elements = drupal_get_form('translate_export_locale_translate_export_selected_po_form', $names);
    $output .= drupal_render($elements);
  }

  return $output;
}

/**
 * Form to export PO files for the languages provided.
 *
 * @param $names
 *   An associate array with localized language names
 */
function translate_export_locale_translate_export_selected_po_form($form, &$form_state, $names) {
  $form['export_title'] = array('#type' => 'item',
    '#title' => t('Export translation selected'),
  );
  $form['langcode'] = array('#type' => 'select',
    '#title' => t('Language name'),
    '#options' => $names,
    '#description' => t('Select the language to export in Gettext Portable Object (<em>.po</em>) format.'),
  );
  $form['group'] = array('#type' => 'radios',
    '#title' => t('Text group'),
    '#default_value' => 'default',
    '#options' => module_invoke_all('locale', 'groups'),
  );
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Export'));
  return $form;
}

/**
 * Process a translation (or template) export form submission.
 */
function translate_export_locale_translate_export_selected_po_form_submit($form, &$form_state) {
  // If template is required, language code is not given.
  $language = NULL;
  if (isset($form_state['values']['langcode'])) {
    $languages = language_list();
    $language = $languages[$form_state['values']['langcode']];
  }
  _locale_export_po($language, _locale_export_po_generate($language, _translate_export_locale_export_selected_get_strings($language, $form_state['values']['group'])));
}

function translate_export_locale_translate_seek_form($form, &$form_state, $lids){
	$result = db_query('select lid from {translate_export_selected}');

	$lids_selected = array();
	foreach ($result as $record) {
		$lids_selected[] = $record->lid;
	}
	$form['#true'] = TRUE;
	foreach($lids as  $lid){
		if(in_array($lid, $lids_selected)){
			$form['lids'][$lid] = array(
        '#type' => 'button',
        '#value' => t('deselect'),
        '#name' => 'deselect-'.$lid,
        '#ajax' => array(
        	'wrapper' => '#translate-export-locale-translate-seek-form',
        	'callback' => 'translate_export_locale_translate_seek_form_deselect_ajax',
        ),
			);
		}else{
			$form['lids'][$lid] = array(
        '#type' => 'button',
        '#value' => t('select'),
        '#name' => 'select-'.$lid,
        '#ajax' => array(
        	'wrapper' => '#translate-export-locale-translate-seek-form',
        	'callback' => 'translate_export_locale_translate_seek_form_select_ajax',
        ),
			);
		}
	}

  return $form;
}

function translate_export_locale_translate_seek_form_deselect_ajax($form, &$form_state){
  $name = $form_state['triggering_element']['#name'];
  $name_array = explode('-',$name);
  if(isset($name_array[1])){
  	$lid = $name_array[1];
  	$export = (object)array('lid'=>$lid);
  	$query = db_query('delete from translate_export_selected where lid = :lid',array(':lid'=>$lid));
  	$commands =array();
  	$form_new = drupal_get_form('translate_export_locale_translate_seek_form', array_keys($form['lids']));
  	$commands[] = ajax_command_replace('#translate-export-locale-translate-seek-form input[name="deselect-'.$lid.'"]',drupal_render($form_new['lids'][$lid]));
  	return array('#type'=>'ajax', '#commands' => $commands);
  }
}

function translate_export_locale_translate_seek_form_select_ajax($form, &$form_state){
  
  $name = $form_state['triggering_element']['#name'];
  $name_array = explode('-',$name);
  if(isset($name_array[1])){
  	$lid = $name_array[1];
  	$export = (object)array('lid'=>$lid);
  	drupal_write_record('translate_export_selected',$export);
  	$commands =array();
  	$form_new = drupal_get_form('translate_export_locale_translate_seek_form', array_keys($form['lids']));
  	$commands[] = ajax_command_replace('#translate-export-locale-translate-seek-form input[name="select-'.$lid.'"]',drupal_render($form_new['lids'][$lid]));
  	return array('#type'=>'ajax', '#commands' => $commands);
  }
  
}

function translate_export_locale_translate_export_selected_edit(){

	module_load_include('inc', 'locale', 'locale.admin');
  // Add CSS.
  drupal_add_css(drupal_get_path('module', 'locale') . '/locale.css');
  drupal_add_css(drupal_get_path('module', 'translate_export') . '/translate_export.css');
	$output = '<p>'.t('clear all selected strings.').'</p>';
	$form = drupal_get_form('translate_export_locale_translate_export_selected_edit_form');
	$output .= drupal_render($form);
  $output .= _translate_export_locale_translate_selected_seek();

  return $output;
}

function translate_export_locale_translate_export_selected_edit_form($form, &$form_state){
  $form['delete'] = array(
    '#type' => 'submit',
    '#value' => t('Clear'),
  );

  return $form;
}

function translate_export_locale_translate_export_selected_edit_form_submit($form, &$form_state){

  $query = db_query('TRUNCATE TABLE translate_export_selected');

}