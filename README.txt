
# Translate Export

Translate export module is helpful to export strings. it supports export single string, select multi strings to export. it's useful when you need export strings frequently and don't want to export whole group of strings.

## Features

1) in admin/config/regional/translate/translate page, added export and select/deselect operations for each string. click export can export single string in po file. select/deselect can select/deselect string to list for export

2) in admin/config/regional/translate/export-selected page, can export selected strings by group

3) in admin/config/regional/translate/edit-selected page, can edit/clear the selected string


